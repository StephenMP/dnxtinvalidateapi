﻿using DnxTinValidateApi.Dtos;

namespace DnxTinValidateApi.Providers {

	public interface ITinValidateProvider {

		#region Public Methods

		TinValidateResult ValidateTin(TaxIdNumber tin, bool isTest);

		#endregion Public Methods
	}
}
