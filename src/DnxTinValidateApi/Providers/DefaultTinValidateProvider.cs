﻿using DnxTinValidateApi.Dtos;
using DnxTinValidateApi.Enums;
using DnxTinValidateApi.Helpers;

namespace DnxTinValidateApi.Providers {

	public static class DefaultTinValidateProvider {

		#region Public Methods

		public static TinValidateResult ValidateTin(TaxIdNumber tin, bool isTest = true) {
			if (tin == null) {
				return new TinValidateResult {
					IsValid = false,
					IsTest = isTest,
					Message = $"{nameof(tin)} was null"
				};
			}

			return ValidateTin(tin.Tin, tin.TaxNumberType, isTest);
		}

		#endregion Public Methods

		#region Private Methods

		private static TinValidateResult ValidateTin(string tin, TaxNumberType taxNumberType, bool isTest) {
			switch (taxNumberType) {
				case TaxNumberType.AustralianBusinessNumber:
					return AustralianBusinessNumberHelper.Validate(tin);

				case TaxNumberType.SocialInsuranceNumber:
					return SocialInsuranceNumberHelper.Validate(tin);

				case TaxNumberType.TaxFileNumber:
					return TaxFileNumberHelper.Validate(tin);

				default:
					return new TinValidateResult {
						IsValid = true,
						IsTest = isTest,
						Message = $"Unrecognized tax number type: {taxNumberType}"
					};
			}
		}

		#endregion Private Methods
	}
}
