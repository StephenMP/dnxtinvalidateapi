﻿using DnxTinValidateApi.Dtos;
using DnxTinValidateApi.TinCheck;
using Microsoft.Extensions.OptionsModel;

namespace DnxTinValidateApi.Providers {

	public class TinCheckTinValidateProvider {

		#region Private Fields

		private TinCheckWrapper tinCheck;

		#endregion Private Fields

		#region Public Constructors

		public TinCheckTinValidateProvider(IOptions<Settings> optionsAccessor, TinCheckWrapper wrapper = null) {
			this.tinCheck = wrapper ?? new TinCheckWrapper(optionsAccessor);
		}

		#endregion Public Constructors

		#region Public Methods

		public TinValidateResult ValidateTin(TaxIdNumber tin) {
			if (tin == null) {
				return new TinValidateResult {
					IsValid = false,
					Message = $"{nameof(tin)} was null"
				};
			}

			var tinCheckResult = tinCheck.ValidateTin(tin.Tin, tin.LegalName.ToString());

			return new TinValidateResult {
				IsValid = tinCheckResult.IsValid,
				Message = "DNX Tin Validate baby!"
			};
		}

		#endregion Public Methods
	}
}
