﻿namespace DnxTinValidateApi.Enums {

	public enum TaxNumberType {
		Unknown = 0,
		SocialSecurityNumberUnitedStates = 1,
		ValueAddedTaxId = 2,
		EmployerIdNumber = 3,
		PersonalIdentityNumber = 4,
		NationalInsuranceNumber = 5,
		PassportNumber = 6,
		SocialInsuranceNumber = 7,
		PersonalPublicServiceNumber = 9,
		ClaveUnicaDeRegistroDePoblacion = 10,
		AustralianBusinessNumber = 11,
		DocumentoNacionalDeIdentidad = 12,
		PowszechnyElektronicznySystemEwidencjiLudnosci = 13,
		GoodsAndServicesTax = 14,
		RegistroFederaldeContribuyentes = 15,
		TaxFileNumber = 16,
		SocialSecurityNumberFrance = 17,
		Sozialversicherungsnummer = 18,
		NumerodeIdentidaddeExtranjero = 19,
		BusinessNumber = 20,
		InlandRevenueDescription = 21
	}
}
