﻿namespace DnxTinValidateApi.Enums {

	public enum Market {
		Unknown = 0,
		USA,
		Canada,
		Germany,
		UK,
		Poland,
		Netherlands,
		Portugal,
		Ireland,
		Austria,
		France,
		Italy,
		Luxembourg,
		Spain,
		Mexico,
		Australia,
		NewZealand
	}
}
