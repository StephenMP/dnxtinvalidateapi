﻿namespace DnxTinValidateApi.Enums {

	public enum ServiceResultCode {
		Unknown = 0,
		InvalidData = 11,
		InvalidConfiguration = 12,
		RequestDeniedNoMoreChecksAvailableForThisAccount = 18,
		ProcessingError = 20,
		LogOnDeniedInvalidUser = 23,
		LogOnDeniedInvalidPassword = 24,
		LogOnDeniedThisAccountHasBeenLocked = 25,
		LogOnDeniedAccountLockedFor24Hours3OrMoreLogOnAttempts = 26,
		LogOnDeniedTermsNotAccepted = 27,
		LogOnDeniedAccountExpired = 28,
		LogOnDeniedNoSecurityRights = 29
	}
}
