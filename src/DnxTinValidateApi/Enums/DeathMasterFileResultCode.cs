﻿namespace DnxTinValidateApi.Enums {

	public enum DeathMasterFileResultCode {
		DeathMasterFileMatchNotProcessed = -1,
		NoDeathMasterFileMatchFound = 0,
		PossibleDeathMasterFileMatchFound = 1,
		LogOnDeniedInvalidUserLogOnAndOrPassword = 10,
		InvalidDeathMasterFileMatchingRequest = 13,
		ConnectionProblem = 15,
		DeathMasterFileConnectionProblem = 16,
		DeathMasterFileMatchTemporarilyUnavailable = 17,
		DeathMasterFileProcessingError = 21
	}
}
