﻿namespace DnxTinValidateApi.Enums {

	public enum TinNameResultCode {
		TinNameValidationNotProcessed = -1,
		TinNameCombinationDoesNotMatchIrsRecords = 0,
		TinAndNameCombinationMatchesIrsRecords = 1,
		TinEnteredIsNotCurrentlyIssued = 5,
		TinAndNameCombinationMatchesIrsSsnRecords = 6,
		TinAndNameCombinationMatchesIrsEinRecords = 7,
		TinAndNameCombinationMatchesIrsSsnAndEinRecords = 8,
		LogOnDeniedInvalidUserLogOnAndOrPassword = 10,
		InvalidTinMatchingRequest = 13,
		DuplicateTinMatchingRequest = 14,
		ConnectionProblem = 15,
		IrsConnectionProblem = 16,
		IrsTinNameValidationTemporarilyUnavailable = 17,
		IrsProcessingError = 21,
		InvalidIrsLogOn = 22
	}
}
