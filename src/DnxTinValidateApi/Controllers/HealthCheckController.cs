﻿using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.OptionsModel;

namespace DnxTinValidateApi.Controllers {
	[Route("api/[controller]")]
	public class HealthCheckController : Controller {

		#region Public Methods
		private Settings settings;

		public HealthCheckController(IOptions<Settings> optionsAccessor) {
			this.settings = optionsAccessor.Value;
		}

		[HttpGet]
		public IActionResult Get() {
			var result = new HealthCheckResult {
				Message = "All systems go!",
				ShimUrl = this.settings.TinValidateShimApiUrl
			};

			return Ok(result);
		}

		#endregion Public Methods
	}

	internal class HealthCheckResult {
		public string Message { get; set; }
		public string ShimUrl { get; set; }
	}
}
