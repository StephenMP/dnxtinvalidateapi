﻿using System.Security.Cryptography;
using DnxTinValidateApi.Dtos;
using DnxTinValidateApi.Enums;
using DnxTinValidateApi.Providers;
using DnxTinValidateApi.ScentsyFamily;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.OptionsModel;

namespace DnxTinValidateApi.Controllers {
	[Route("api/[controller]")]
	public class ValidateController : Controller {
		#region Private Fields

		private bool isTest;
		private TinCheckTinValidateProvider usaTinValidateProvider;
		private Settings settings;

		#endregion Private Fields

		#region Public Constructors

		public ValidateController(IOptions<Settings> optionsAccessor) {
			this.isTest = true;
			this.settings = optionsAccessor.Value;
			this.usaTinValidateProvider = new TinCheckTinValidateProvider(optionsAccessor);
		}

		#endregion Public Constructors

		#region Public Methods
		[HttpGet]
		public IActionResult Get() {
			return Ok("Sweetness");
		}

		[HttpGet("/api/[controller]/ValidateTin")]
		public IActionResult ValidateTin(string tin, Market market, TaxNumberType taxNumberType, string name) {
			try {
				if (string.IsNullOrWhiteSpace(tin) || market == Market.Unknown || taxNumberType == TaxNumberType.Unknown || string.IsNullOrWhiteSpace(name)) {
					return Ok(new TinValidateResult {
						IsTest = this.isTest,
						IsValid = false,
						Message = $"{nameof(tin)}, {nameof(market)}, {nameof(taxNumberType)}, or {nameof(name)} was null"
					});
				}

				var taxIdNumber = new TaxIdNumber {
					Tin = tin,
					Market = market,
					LegalName = new LegalName {
						LastName = name
					},
					TaxNumberType = taxNumberType
				};

				if (market == Market.USA && (taxNumberType == TaxNumberType.SocialSecurityNumberUnitedStates || taxNumberType == TaxNumberType.EmployerIdNumber)) {
					return Ok(this.usaTinValidateProvider.ValidateTin(taxIdNumber));
				}
				else {
					return Ok(DefaultTinValidateProvider.ValidateTin(taxIdNumber, this.isTest));
				}
			}

			catch {
				throw;
			}
		}

		#endregion Public Methods
	}
}
