﻿using DnxTinValidateApi.Enums;
using DnxTinValidateApi.ScentsyFamily;
using DnxTinValidateApi.ScentsyFamily.Extensions;

namespace DnxTinValidateApi.Dtos {

	public class TaxIdNumber : Equatable<TaxIdNumber> {

		#region Public Constructors

		public TaxIdNumber() {
			Tin = string.Empty;
			LegalName = new LegalName();
			TaxNumberType = TaxNumberType.Unknown;
			Market = Market.Unknown;
		}

		public TaxIdNumber(string taxIdNumber, LegalName legalName) : this() {
			this.Tin = taxIdNumber;
			this.LegalName = legalName;
		}

		#endregion Public Constructors

		#region Public Properties

		public LegalName LegalName { get; set; }
		public Market Market { get; set; }
		public TaxNumberType TaxNumberType { get; set; }
		public string Tin { get; set; }

		#endregion Public Properties

		#region Public Methods

		public override int GetHashCode() {
			var hash = Tin.GetHashCodeSafe();
			hash ^= LegalName.GetHashCodeSafe();
			hash ^= Market.GetHashCodeSafe();
			hash ^= TaxNumberType.GetHashCodeSafe();

			return hash;
		}

		#endregion Public Methods
	}
}
