﻿using DnxTinValidateApi.ScentsyFamily;
using DnxTinValidateApi.ScentsyFamily.Extensions;
using DnxTinValidateApi.TinCheck;

namespace DnxTinValidateApi.Dtos {

	public class TinCheckResult : Equatable<TinCheckResult> {

		#region Public Constructors

		public TinCheckResult() {
			this.DeathMasterFile = new DeathMasterFile();
			this.TinName = new TinName();
			this.IsException = false;
			this.ExceptionMessage = null;
		}

		public TinCheckResult(TinNameResponse response) : this() {
			if (response != null) {
				this.DeathMasterFile.Code = response.DMF_CODE;
				this.DeathMasterFile.Data = response.DMF_DATA;
				this.DeathMasterFile.Details = response.DMF_DETAILS;
				this.TinName.Code = response.TINNAME_CODE;
				this.TinName.Details = response.TINNAME_DETAILS;
			}
		}

		#endregion Public Constructors

		#region Public Properties

		public DeathMasterFile DeathMasterFile { get; set; }
		public string ExceptionMessage { get; set; }
		public bool IsException { get; set; }

		public bool IsValid {
			get {
				return !IsException && DeathMasterFile.IsValid && TinName.IsValid;
			}
		}

		public string Message {
			get {
				if (this.IsValid) {
					return "";
				}

				if (this.IsException) {
					return $"Exception Details:\n\t{ExceptionMessage}";
				}

				return $"TinName Message:\n\t{this.TinName.Details}\nDmf Message\n\t{this.DeathMasterFile.Details}\n\t{this.DeathMasterFile.Data}";
			}
		}

		public TinName TinName { get; set; }

		#endregion Public Properties

		#region Public Methods

		public override int GetHashCode() {
			var hash = this.IsValid.GetHashCodeSafe();

			hash ^= this.TinName.GetHashCodeSafe();
			hash ^= this.DeathMasterFile.GetHashCodeSafe();
			hash ^= this.Message.GetHashCodeSafe();

			return hash;
		}

		#endregion Public Methods
	}
}
