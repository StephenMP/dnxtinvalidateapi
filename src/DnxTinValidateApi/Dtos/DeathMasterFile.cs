﻿using DnxTinValidateApi.Enums;
using DnxTinValidateApi.ScentsyFamily;
using DnxTinValidateApi.ScentsyFamily.Extensions;

namespace DnxTinValidateApi.Dtos {

	public class DeathMasterFile : Equatable<DeathMasterFile> {

		#region Public Properties

		public sbyte Code { get; set; }
		public string Data { get; set; }
		public string Details { get; set; }

		public bool IsValid {
			get {
				return this.Code == (int)DeathMasterFileResultCode.NoDeathMasterFileMatchFound;
			}
		}

		#endregion Public Properties

		#region Public Methods

		public override int GetHashCode() {
			var hash = this.Code.GetHashCodeSafe();

			hash ^= this.Details.GetHashCodeSafe();
			hash ^= this.Data.GetHashCodeSafe();

			return hash;
		}

		#endregion Public Methods
	}
}
