﻿using DnxTinValidateApi.ScentsyFamily;
using DnxTinValidateApi.ScentsyFamily.Extensions;

namespace DnxTinValidateApi.Dtos {

	public class LegalName : Equatable<LegalName> {
		// TODO: Use CoreProxy LegalName object instead

		#region Public Properties

		public string FirstName { get; set; }
		public string LastName { get; set; }

		#endregion Public Properties

		#region Public Methods

		public override int GetHashCode() {
			var hash = FirstName.GetHashCodeSafe();
			hash ^= LastName.GetHashCodeSafe();

			return hash;
		}

		public override string ToString() {
			return string.IsNullOrEmpty(FirstName) ? LastName : $"{FirstName} {LastName}";
		}

		#endregion Public Methods
	}
}
