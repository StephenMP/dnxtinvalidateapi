﻿using DnxTinValidateApi.ScentsyFamily;
using DnxTinValidateApi.ScentsyFamily.Extensions;

namespace DnxTinValidateApi.Dtos {

	public class TinValidateResult : Equatable<TinValidateResult> {

		#region Public Properties

		public bool IsTest { get; set; }
		public bool IsValid { get; set; }
		public string Message { get; set; }

		#endregion Public Properties

		#region Public Methods

		public override int GetHashCode() {
			var hash = IsValid.GetHashCodeSafe();
			hash ^= IsTest.GetHashCodeSafe();
			hash ^= Message.GetHashCodeSafe();

			return hash;
		}

		#endregion Public Methods
	}
}
