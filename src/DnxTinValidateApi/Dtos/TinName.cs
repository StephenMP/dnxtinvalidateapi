﻿using DnxTinValidateApi.Enums;
using DnxTinValidateApi.ScentsyFamily;
using DnxTinValidateApi.ScentsyFamily.Extensions;

namespace DnxTinValidateApi.Dtos {

	public class TinName : Equatable<TinName> {

		#region Public Properties

		public sbyte Code { get; set; }
		public string Details { get; set; }

		public bool IsValid {
			get {
				return this.Code == (int)TinNameResultCode.TinAndNameCombinationMatchesIrsRecords
					  || this.Code == (int)TinNameResultCode.TinAndNameCombinationMatchesIrsSsnRecords
					  || this.Code == (int)TinNameResultCode.TinAndNameCombinationMatchesIrsEinRecords
					  || this.Code == (int)TinNameResultCode.TinAndNameCombinationMatchesIrsSsnAndEinRecords;
			}
		}

		#endregion Public Properties

		#region Public Methods

		public override int GetHashCode() {
			var hash = this.Code.GetHashCodeSafe();

			hash ^= this.Details.GetHashCodeSafe();

			return hash;
		}

		#endregion Public Methods
	}
}
