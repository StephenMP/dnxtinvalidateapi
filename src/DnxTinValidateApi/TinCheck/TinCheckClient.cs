﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Xml.Serialization;

namespace DnxTinValidateApi.TinCheck {

	[GeneratedCodeAttribute("Xml", "4.6.81.0")]
	[SerializableAttribute()]
	[XmlTypeAttribute(Namespace = "http://www.TinCheck.com/WebServices/PVSService/")]
	public enum ServiceStatus {
		LoginFailed,
		OK,
	}

	[GeneratedCodeAttribute("ServiceModel", "4.0.0.0")]
	[ServiceContractAttribute(Namespace = "http://www.TinCheck.com/WebServices/PVSService/", ConfigurationName = "TinCheckService.PVSServiceSoap")]
	public interface IPVSServiceSoap {

		#region Public Methods

		[OperationContractAttribute(Action = "http://www.TinCheck.com/WebServices/PVSService/ServiceStatus", ReplyAction = "*")]
		[XmlSerializerFormatAttribute(SupportFaults = true)]
		ServiceStatusResponse ServiceStatus(UserClass CurUser);

		[OperationContractAttribute(Action = "http://www.TinCheck.com/WebServices/PVSService/ValidateTinName", ReplyAction = "*")]
		[XmlSerializerFormatAttribute(SupportFaults = true)]
		TinNameResponse ValidateTinName(TinNameClass TinName, UserClass CurUser);

		[OperationContractAttribute(Action = "http://www.TinCheck.com/WebServices/PVSService/Version", ReplyAction = "*")]
		[XmlSerializerFormatAttribute(SupportFaults = true)]
		string Version();

		#endregion Public Methods
	}

	[DebuggerStepThroughAttribute()]
	[GeneratedCodeAttribute("ServiceModel", "4.0.0.0")]
	public partial class PVSServiceSoapClient : ClientBase<IPVSServiceSoap> {

		#region Public Constructors

		public PVSServiceSoapClient(Binding binding, EndpointAddress remoteAddress) :
				base(binding, remoteAddress) {
		}

		#endregion Public Constructors

		#region Public Methods

		public ServiceStatusResponse ServiceStatus(UserClass CurUser) {
			return Channel.ServiceStatus(CurUser);
		}

		public TinNameResponse ValidateTinName(TinNameClass TinName, UserClass CurUser) {
			return Channel.ValidateTinName(TinName, CurUser);
		}

		#endregion Public Methods
	}

	[GeneratedCodeAttribute("Xml", "4.6.81.0")]
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(Namespace = "http://www.TinCheck.com/WebServices/PVSService/")]
	public partial class ServiceStatusResponse : object, INotifyPropertyChanged {

		#region Private Fields

		private ServiceStatus statusField;

		#endregion Private Fields

		#region Public Events

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion Public Events

		#region Public Properties

		[XmlElementAttribute(Order = 0)]
		public ServiceStatus Status
		{
			get
			{
				return this.statusField;
			}
			set
			{
				this.statusField = value;
				this.RaisePropertyChanged("Status");
			}
		}

		#endregion Public Properties

		#region Protected Methods

		protected void RaisePropertyChanged(string propertyName) {
			var propertyChanged = this.PropertyChanged;
		}

		#endregion Protected Methods
	}

	[GeneratedCodeAttribute("Xml", "4.6.81.0")]
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(Namespace = "http://www.TinCheck.com/WebServices/PVSService/")]
	public partial class TinNameClass : object, INotifyPropertyChanged {

		#region Private Fields

		private string lNameField;
		private string tINField;

		#endregion Private Fields

		#region Public Events

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion Public Events

		#region Public Properties

		[XmlElementAttribute(Order = 1)]
		public string LName
		{
			get
			{
				return this.lNameField;
			}
			set
			{
				this.lNameField = value;
				this.RaisePropertyChanged("LName");
			}
		}

		[XmlElementAttribute(Order = 0)]
		public string TIN
		{
			get
			{
				return this.tINField;
			}
			set
			{
				this.tINField = value;
				this.RaisePropertyChanged("TIN");
			}
		}

		#endregion Public Properties

		#region Protected Methods

		protected void RaisePropertyChanged(string propertyName) {
			var propertyChanged = this.PropertyChanged;
		}

		#endregion Protected Methods
	}

	[GeneratedCodeAttribute("Xml", "4.6.81.0")]
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(Namespace = "http://www.TinCheck.com/WebServices/PVSService/")]
	public partial class TinNameResponse : object, INotifyPropertyChanged {

		#region Private Fields

		private sbyte dMF_CODEField;
		private string dMF_DATAField;
		private string dMF_DETAILSField;
		private sbyte tINNAME_CODEField;
		private string tINNAME_DETAILSField;

		#endregion Private Fields

		#region Public Events

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion Public Events

		#region Public Properties

		[XmlElementAttribute(Order = 2)]
		public sbyte DMF_CODE
		{
			get
			{
				return this.dMF_CODEField;
			}
			set
			{
				this.dMF_CODEField = value;
				this.RaisePropertyChanged("DMF_CODE");
			}
		}

		[XmlElementAttribute(Order = 4)]
		public string DMF_DATA
		{
			get
			{
				return this.dMF_DATAField;
			}
			set
			{
				this.dMF_DATAField = value;
				this.RaisePropertyChanged("DMF_DATA");
			}
		}

		[XmlElementAttribute(Order = 3)]
		public string DMF_DETAILS
		{
			get
			{
				return this.dMF_DETAILSField;
			}
			set
			{
				this.dMF_DETAILSField = value;
				this.RaisePropertyChanged("DMF_DETAILS");
			}
		}

		[XmlElementAttribute(Order = 0)]
		public sbyte TINNAME_CODE
		{
			get
			{
				return this.tINNAME_CODEField;
			}
			set
			{
				this.tINNAME_CODEField = value;
				this.RaisePropertyChanged("TINNAME_CODE");
			}
		}

		[XmlElementAttribute(Order = 1)]
		public string TINNAME_DETAILS
		{
			get
			{
				return this.tINNAME_DETAILSField;
			}
			set
			{
				this.tINNAME_DETAILSField = value;
				this.RaisePropertyChanged("TINNAME_DETAILS");
			}
		}

		#endregion Public Properties

		#region Protected Methods

		protected void RaisePropertyChanged(string propertyName) {
			var propertyChanged = this.PropertyChanged;
		}

		#endregion Protected Methods
	}

	[GeneratedCodeAttribute("Xml", "4.6.81.0")]
	[SerializableAttribute()]
	[DebuggerStepThroughAttribute()]
	[DesignerCategoryAttribute("code")]
	[XmlTypeAttribute(Namespace = "http://www.TinCheck.com/WebServices/PVSService/")]
	public partial class UserClass : object, INotifyPropertyChanged {

		#region Private Fields

		private string userLoginField;
		private string userPasswordField;

		#endregion Private Fields

		#region Public Events

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion Public Events

		#region Public Properties

		[XmlElementAttribute(Order = 1)]
		public string UserLogin
		{
			get
			{
				return this.userLoginField;
			}
			set
			{
				this.userLoginField = value;
				this.RaisePropertyChanged("UserLogin");
			}
		}

		[XmlElementAttribute(Order = 2)]
		public string UserPassword
		{
			get
			{
				return this.userPasswordField;
			}
			set
			{
				this.userPasswordField = value;
				this.RaisePropertyChanged("UserPassword");
			}
		}

		#endregion Public Properties

		#region Protected Methods

		protected void RaisePropertyChanged(string propertyName) {
			var propertyChanged = this.PropertyChanged;
		}

		#endregion Protected Methods
	}
}
