﻿using DnxTinValidateApi.Dtos;
using Microsoft.Extensions.OptionsModel;

namespace DnxTinValidateApi.TinCheck {

	public class TinCheckWrapper {

		#region Private Fields

		private ITinCheckService tinCheckService;

		#endregion Private Fields

		#region Public Constructors

		public TinCheckWrapper(ITinCheckService tinCheckService) {
			this.tinCheckService = tinCheckService;
		}

		public TinCheckWrapper(IOptions<Settings> optionsAccessor) : this(new TinCheckService(optionsAccessor)) {
		}

		#endregion Public Constructors

		#region Public Methods

		public bool HealthCheck() {
			return this.tinCheckService.HealthCheck();
		}

		public TinCheckResult ValidateTin(string tin, string legalName) {
			if (string.IsNullOrWhiteSpace(tin) || string.IsNullOrWhiteSpace(legalName)) {
				return new TinCheckResult {
					IsException = true,
					ExceptionMessage = $"{nameof(tin)} or {nameof(legalName)} was null or empty."
				};
			}

			if (tin.Length < 9) {
				return new TinCheckResult {
					IsException = true,
					ExceptionMessage = $"{nameof(tin)} was not the correct length."
				};
			}

			return this.tinCheckService.ValidateTin(tin, legalName);
		}

		#endregion Public Methods
	}
}
