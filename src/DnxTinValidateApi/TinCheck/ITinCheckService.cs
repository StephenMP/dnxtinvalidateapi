﻿using DnxTinValidateApi.Dtos;

namespace DnxTinValidateApi.TinCheck {

	public interface ITinCheckService {

		#region Public Methods

		bool HealthCheck();

		TinCheckResult ValidateTin(string tin, string legalName);

		#endregion Public Methods
	}
}
