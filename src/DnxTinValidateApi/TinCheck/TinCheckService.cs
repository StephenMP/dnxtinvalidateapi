﻿using System;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Text;
using DnxTinValidateApi.Dtos;
using Microsoft.Extensions.OptionsModel;

namespace DnxTinValidateApi.TinCheck {

	public class TinCheckService : ITinCheckService {

		#region Private Fields

		private EndpointAddress endpointAddress;
		private BasicHttpsBinding httpsBinding;
		private UserClass userClass;
		private Settings settings;

		#endregion Private Fields

		#region Public Constructors

		public TinCheckService(IOptions<Settings> optionsAccessor) {
			this.settings = optionsAccessor.Value;

			this.userClass = new UserClass {
				UserLogin = this.settings.TinCheckUsername,
				UserPassword = this.settings.TinCheckPassword
			};

			this.endpointAddress = new EndpointAddress(this.settings.TinValidateShimApiUrl + "/shim/");
			this.httpsBinding = new BasicHttpsBinding {
				Name = "PVSServiceSoap",
				CloseTimeout = new TimeSpan(0, 1, 0),
				OpenTimeout = new TimeSpan(0, 1, 0),
				ReceiveTimeout = new TimeSpan(0, 10, 0),
				SendTimeout = new TimeSpan(0, 1, 0),
				AllowCookies = false,
				BypassProxyOnLocal = false,
				HostNameComparisonMode = HostNameComparisonMode.StrongWildcard,
				MaxBufferSize = 65536,
				MaxBufferPoolSize = 524288,
				MaxReceivedMessageSize = 65536,
				MessageEncoding = WSMessageEncoding.Text,
				TextEncoding = Encoding.UTF8,
				TransferMode = TransferMode.Buffered,
				UseDefaultWebProxy = true,
			};

			this.httpsBinding.ReaderQuotas.MaxDepth = 32;
			this.httpsBinding.ReaderQuotas.MaxStringContentLength = 8192;
			this.httpsBinding.ReaderQuotas.MaxArrayLength = 16384;
			this.httpsBinding.ReaderQuotas.MaxBytesPerRead = 4096;
			this.httpsBinding.ReaderQuotas.MaxNameTableCharCount = 16384;
			this.httpsBinding.Security.Mode = BasicHttpsSecurityMode.Transport;
			this.httpsBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
			this.httpsBinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
			this.httpsBinding.Security.Transport.Realm = "";
			this.httpsBinding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
			this.httpsBinding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default;
		}

		public TinCheckService(IOptions<Settings> optionsAccessor, string userLogin, string userPassword) : this(optionsAccessor) {
			this.userClass.UserLogin = userLogin;
			this.userClass.UserPassword = userPassword;
		}

		#endregion Public Constructors

		#region Public Methods

		public bool HealthCheck() {
			return true;
		}

		public TinCheckResult ValidateTin(string tin, string legalName) {
			return new TinCheckResult {
				DeathMasterFile = new DeathMasterFile { Code = 0, Data = "", Details = "" },
				TinName = new TinName { Code = 1, Details = "" },
				ExceptionMessage = "",
				IsException = false
			};
		}

		#endregion Public Methods

		#region Private Methods

		private static TinNameClass BuildTinNameClass(string tin, string legalName) {
			return new TinNameClass {
				TIN = tin,
				LName = legalName,
			};
		}

		#endregion Private Methods
	}
}
