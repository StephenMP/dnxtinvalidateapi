﻿using System.Security.Cryptography;
using System.Text;

namespace DnxTinValidateApi {
	public class Settings {
		public string TinCheckUsername { get; set; }
		public string TinCheckPassword { get; set; }
		public string TinValidateShimApiUrl { get; set; }
	}
}
