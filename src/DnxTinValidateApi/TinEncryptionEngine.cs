﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace DnxTinValidateApi {

	public static class TinEncryptionEngine {

		#region Public Methods

		public static string Decrypt(string encryptedValue, RSAParameters privateKey) {
			return EncryptOrDecrypt(encryptedValue, privateKey, Convert.FromBase64String, (c, b) => Encoding.UTF8.GetString(c.Decrypt(b, true)));
		}

		public static string Encrypt(string value, RSAParameters publicKey) {
			return EncryptOrDecrypt(value, publicKey, Encoding.UTF8.GetBytes, (c, b) => Convert.ToBase64String(c.Encrypt(b, true)));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public static RSAParameters GetNewPrivateKey() {
			using (var crypto = new RSACryptoServiceProvider(512)) {
				return crypto.ExportParameters(true);
			}
		}

		public static RSAParameters GetPublicKeyFromPrivateKey(RSAParameters privateKey) {
			using (var crypto = GetCryptoFromString(privateKey)) {
				return crypto.ExportParameters(false);
			}
		}

		#endregion Public Methods

		#region Private Methods

		private static string EncryptOrDecrypt(string value, RSAParameters key, Func<string, byte[]> getBytes, Func<RSACryptoServiceProvider, byte[], string> function) {
			using (var crypto = GetCryptoFromString(key)) {
				var bytes = getBytes(value);
				return function(crypto, bytes);
			}
		}

		private static RSACryptoServiceProvider GetCryptoFromString(RSAParameters key) {
			RSACryptoServiceProvider crypto = null;

			try {
				crypto = new RSACryptoServiceProvider();
				crypto.ImportParameters(key);
				return crypto;
			}
			catch (Exception) {
				if (crypto != null) {
					crypto.Dispose();
				}

				throw;
			}
		}

		#endregion Private Methods
	}
}
