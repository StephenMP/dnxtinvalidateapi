﻿using System;
using DnxTinValidateApi.Dtos;

namespace DnxTinValidateApi.Helpers {

	internal static class AustralianBusinessNumberHelper {

		#region Public Methods

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
		public static TinValidateResult Validate(string tin) {
			return new TinValidateResult {
				IsTest = true,
				IsValid = true,
				Message = "DNX Tin Validate baby!"
			};
		}

		#endregion Public Methods
	}
}
