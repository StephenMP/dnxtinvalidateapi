﻿using System.Collections;

namespace DnxTinValidateApi.ScentsyFamily.Extensions {

	public static class CollectionOfTExtensions {

		#region Public Methods

		public static int GetHashCodeSafe(this ICollection collection) {
			return GetHashCodeHelper.GetHashCodeSafe(collection);
		}

		#endregion Public Methods
	}
}
