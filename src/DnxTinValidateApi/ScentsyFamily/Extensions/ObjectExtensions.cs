﻿namespace DnxTinValidateApi.ScentsyFamily.Extensions {

	public static class ObjectExtensions {

		#region Public Methods

		public static int GetHashCodeSafe(this object value) {
			return GetHashCodeHelper.GetHashCodeSafe(value);
		}

		#endregion Public Methods
	}
}
