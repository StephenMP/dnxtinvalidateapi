﻿using System.Collections;

namespace DnxTinValidateApi.ScentsyFamily {

	public static class GetHashCodeHelper {

		#region Public Methods

		public static int GetHashCodeSafe(object value) {
			if (value == null) {
				return 0;
			}

			return value.GetHashCode();
		}

		public static int GetHashCodeSafe(ICollection collection) {
			int result = 0;

			if (collection == null) {
				return result;
			}

			foreach (var item in collection) {
				result ^= GetHashCodeSafe(item);
			}

			return result;
		}

		#endregion Public Methods
	}
}
