﻿using System;

namespace DnxTinValidateApi.ScentsyFamily {

	public abstract class Equatable<T> : IEquatable<T> where T : class, IEquatable<T> {

		#region Public Methods

		public static bool operator !=(Equatable<T> leftHandSide, Equatable<T> rightHandSide) {
			return !(leftHandSide == rightHandSide);
		}

		public static bool operator ==(Equatable<T> leftHandSide, Equatable<T> rightHandSide) {
			if (ReferenceEquals(leftHandSide, null)) {
				return ReferenceEquals(rightHandSide, null);
			}

			return leftHandSide.Equals(rightHandSide);
		}

		public bool Equals(T other) {
			if (ReferenceEquals(other, null)) {
				return false;
			}

			return this.GetHashCode() == other.GetHashCode();
		}

		public override bool Equals(object obj) {
			return Equals(obj as T);
		}

		public abstract override int GetHashCode();

		#endregion Public Methods
	}
}
