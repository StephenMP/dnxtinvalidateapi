﻿using System;
using DnxTinValidateApi;
using DnxTinValidateApi.Controllers;
using DnxTinValidateApi.Enums;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.OptionsModel;
using Moq;
using Xunit;

namespace DnxTinValidate.Tests.Controllers.Component {
	internal class ValidateControllerSteps {
		private Mock<IOptions<Settings>> mockOptionsAccessor;
		private Settings settings;
		private ValidateController controller;
		private Action action;
		private IActionResult result;

		internal void GivenIHaveAValidateController() {
			this.controller = new ValidateController(mockOptionsAccessor.Object);
		}

		internal void GivenIHaveTheFollowingSettings(string username, string password, string shimUri) {
			this.settings = new Settings {
				TinCheckUsername = username,
				TinCheckPassword = password,
				TinValidateShimApiUrl = shimUri
			};
		}

		internal void GivenIHaveOptionsToInject() {
			this.mockOptionsAccessor = new Mock<IOptions<Settings>>();
			this.mockOptionsAccessor.Setup(o => o.Value).Returns(this.settings);
		}

		internal void ThenICanVerifyIThrewAnException<T>() where T : Exception {
			Assert.Throws<T>(this.action);
		}

		internal void WhenICreateTheController() {
			this.action = () => { this.controller = new ValidateController(this.mockOptionsAccessor.Object); };
		}

		internal void ThenICanVerifyIGotAResult() {
			Assert.NotNull(result);
		}

		internal void WhenICallValidateTin(string tin, Market market, TaxNumberType taxNumberType, string name) {
			result = this.controller.ValidateTin(tin, market, taxNumberType, name);
		}

		internal void ThenICanVerifyIHaveAValidateController() {
			Assert.NotNull(controller);
		}
	}
}