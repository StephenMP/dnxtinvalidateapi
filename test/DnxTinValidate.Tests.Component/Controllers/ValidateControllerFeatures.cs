﻿using System;
using DnxTinValidateApi.Enums;
using Xunit;

namespace DnxTinValidate.Tests.Controllers.Component {
	public class ValidateControllerFeatures
    {
		private ValidateControllerSteps steps = new ValidateControllerSteps();

		[Fact]
		public void CanCreateValidateController() {
			this.steps.GivenIHaveTheFollowingSettings("testuser", "testpass", "https://www.this.is/a/test");
			this.steps.GivenIHaveOptionsToInject();
			this.steps.GivenIHaveAValidateController();
			this.steps.ThenICanVerifyIHaveAValidateController();
		}

		[Fact]
		public void CannotCreateValidateControllerWithBadUri() {
			this.steps.GivenIHaveTheFollowingSettings("testuser", "testpass", "baduri");
			this.steps.GivenIHaveOptionsToInject();
			this.steps.WhenICreateTheController();
			this.steps.ThenICanVerifyIThrewAnException<UriFormatException>();
		}

		[Fact]
		public void CanGetResult() {
			this.steps.GivenIHaveTheFollowingSettings("testuser", "testpass", "https://tinvalidateshim.scentsydvt.com");
			this.steps.GivenIHaveOptionsToInject();
			this.steps.GivenIHaveAValidateController();
			this.steps.WhenICallValidateTin("111111111", Market.Australia, TaxNumberType.AustralianBusinessNumber, "name");
			this.steps.ThenICanVerifyIGotAResult();
		}
	}
}
