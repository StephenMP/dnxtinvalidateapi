FROM microsoft/aspnet:latest

EXPOSE 5000  
ENTRYPOINT ["dnx", "-p", "src/DnxTinValidateApi/project.json", "web"]

COPY src/DnxTinValidateApi/project.json /app/  
WORKDIR /app  
RUN ["dnu", "restore"]  
COPY . /app 
